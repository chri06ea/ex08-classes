﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ice_cream_shop
{
    public class Program
    {
        static void Main(string[] args)
        {
        }
        public static int[] IceCreamShop(int v, int[] arr)
        {
            for (int is1 = 0; is1 < arr.Length; is1++)
                for (int is2 = 0; is2 < arr.Length; is2++)
                    if (arr[is1] + arr[is2] == v && is1 != is2)
                        return new int[] { is1, is2 };
            return new int[] { 0 };
        }
    }
}
