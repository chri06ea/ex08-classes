﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ice_cream_shop;

namespace unit_test
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void IceCreamShopTest1()
        {
            int[] arr = { 1, 4, 5, 3, 2 };
            CollectionAssert.AreEqual(new int[] { 0, 3 }, Program.IceCreamShop(4, arr));
        }
        [TestMethod]
        public void IceCreamShopTest2()
        {
            int[] arr = { 2, 2, 4, 3 };
            CollectionAssert.AreEqual(new int[] { 0, 1 }, Program.IceCreamShop(4, arr));
        }

    }
}
