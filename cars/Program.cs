﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cars
{
    class Program
    {
        static void Main(string[] args)
        {
            /* Opgave 3.2 */
            Car car1 = new Car();
            car1.producent = "bmw";
            car1.model = "i8";
            car1.automatGear = false;
            car1.brændstof = "benzin";
            car1.træk = "firhjulstrukket";

            Console.WriteLine("Producent " + car1.producent + " model " + car1.model + 
                " automatgear " + car1.automatGear + " brændstof " + car1.brændstof + " træk " + car1.træk);

            Car car2 = new Car();
            car2.automatGear = false;
            car2.producent = "ferrari";
            car2.model = "z50";
            car2.brændstof = "benzin";
            car2.træk = "baghjulstrukket";

            Console.WriteLine("Producent " + car2.producent + " model " + car2.model + 
                " automatgear " + car2.automatGear + " brændstof " + car2.brændstof + " træk " + car2.træk);

            Car car3 = new Car();
            car3.automatGear = true;
            car3.producent = "audi";
            car3.model = "q8";
            car3.brændstof = "diesel";
            car3.træk = "firhjulstrukket";

            Console.WriteLine("Producent " + car3.producent + " model " + car3.model + 
                " automatgear " + car3.automatGear + " brændstof " + car3.brændstof + " træk " + car3.træk);

            Console.ReadKey();
        }
    }
}
