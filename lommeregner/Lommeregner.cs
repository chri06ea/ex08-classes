﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lommeregner
{
    public class Lommeregner
    {
        public static double Add(double v1, double v2)
        {
            return v1 + v2;
        }
        public static double Divide(double v1, double v2)
        {
            return v1 / v2;
        }
        public static double Subtract(double v1, double v2)
        {
            return v1 - v2;
        }
        public static double Multiply(double v1, double v2)
        {
            return v1 * v2;
        }
    }
}
